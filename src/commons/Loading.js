import React from 'react';
import Loader from 'react-loader-spinner'

const Loading = () => {
    return (
        <div style={{width: "100%", textAlign: "center", paddingTop: "30px"}}>
            <Loader type="Oval" color="#00BFFF" height={80} width={80} />
        </div>
    )
}

export default Loading
