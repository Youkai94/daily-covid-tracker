import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Home from './pages/Home'
import Header from './commons/Header'

function App() {
  return (
    <div>
      <Header />
      <Home />
    </div>
  );
}

export default App;
