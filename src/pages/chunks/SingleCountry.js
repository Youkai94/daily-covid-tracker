import React, { useState, useEffect } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from "react-bootstrap/Form";
import Chart from 'react-apexcharts';
import '../../commons/Style.css'


function SingleCountry(props) {
  const [selectedCountry, setSelectedCountry] = useState("");

  const populateCountries = props.countries.map((elem, i) => {
    return (
      <option key={i} value={elem.country}>
        {elem.country}
      </option>
    );
  });
  const findCountry = props.countries.filter((elem) => {
    return elem.country === selectedCountry;
  });

  const mapCountryData = findCountry.map((elem, i) => {
    return (
      <div key={i}>
      <Row style={{ paddingBottom: "2rem" }}>
          <Col className="text-center title" sm={12} md={12}>
            <img src={elem.countryInfo.flag} style={{ width: "30px", height: "25px", marginRight: "15px" }} />  
            <span style={{ textTransform: "uppercase" }}>{elem.country}</span>
          </Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Totali: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.cases}</Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Attivi: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.active}</Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Decessi: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.deaths}</Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Guariti: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.recovered}</Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Casi giornalieri: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.todayCases}</Col> 
        </Row> 
        <Row className="cRow">
          <Col className="text-left" sm={6} md={6}><b>Decessi giornalieri: </b></Col> 
          <Col className="text-right" sm={6} md={6}>{elem.todayDeaths}</Col> 
        </Row> 
        </div>
    );
  });

  const buildPie = findCountry.map((elem, i) =>{
    const active = elem.active;
    const deaths = elem.deaths;
    const recovered = elem.recovered;
    return ({
      series: [active, deaths, recovered],
      options: {
        chart: {
          width: 380,
          type: 'pie',
        },
        labels: ['Attivi', 'Decessi', 'Guariti'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
    });
  });

  console.log(findCountry);
  return (
    <>
      <Container>
        <Row style={{ paddingTop: "2rem" }}>
          <Col sm={12} md={12}>
            <Form>
              <Form.Group as={Row} controlId="test">
                <Form.Control
                  as="select"
                  value={selectedCountry}
                  onChange={(e) => setSelectedCountry(e.target.value)}
                >
                  <option>Select Country...</option>
                  {populateCountries}
                </Form.Control>
              </Form.Group>
            </Form>
          </Col>
        </Row>
        <Row style={{ paddingTop: "2rem" }}>
            <Col className="countryDataTab" sm={12} md={4}>
              {selectedCountry !== "" && (
                mapCountryData
              )}
            </Col>
            <Col className="text-center" sm={12} md={{span: 6, offset: 1}}>
              {selectedCountry !== "" && (
                <Chart options={buildPie[0].options} series={buildPie[0].series} type="pie" width={400} />
              )}
            </Col>
        </Row>
      </Container>
    </>
  );
}

export default SingleCountry;
