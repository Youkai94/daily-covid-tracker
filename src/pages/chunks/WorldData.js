import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import React from 'react'

function WorldData(props) {
    const NumTextStyle = {
        fontSize: '3rem',
        textAlign: "center",
    }
    return (
        <Container>
            <Row style={{ paddingTop: '2rem' }}>
                <Col sm={12} md={4}>
                    <Card border="secondary">
                        <Card.Header className="text-white" style={{ backgroundColor: "#6c757d", fontSize: '1.4rem', textAlign: "center" }}>Casi Totali</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-secondary" style={NumTextStyle}>{props.data.cases}</Card.Title>
                        
                        </Card.Body>
                    </Card>
                </Col>
                <Col sm={12} md={4}>
                    <Card border="danger">
                        <Card.Header className="text-white" style={{ backgroundColor: "#dc3545", fontSize: '1.4rem', textAlign: "center" }}>Decessi Totali</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-danger" style={NumTextStyle}>{props.data.deaths}</Card.Title>
                        
                        </Card.Body>
                    </Card>
                </Col>
                <Col sm={12} md={4}>
                    <Card border="success">
                        <Card.Header className="text-white" style={{ backgroundColor: "#28a745", fontSize: '1.4rem', textAlign: "center" }}>Guariti Totali</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-success" style={NumTextStyle}>{props.data.recovered}</Card.Title>
                        
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <Row style={{ paddingTop: '2rem' }}>
                <Col sm={12} md={{span: 4, offset: 1}}>
                    <Card border="secondary">
                        <Card.Header className="text-white" style={{ backgroundColor: "#6c757d", fontSize: '1.4rem', textAlign: "center" }}>Casi Giornalieri</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-secondary" style={NumTextStyle}>{props.data.todayCases}</Card.Title>
                        
                        </Card.Body>
                    </Card>
                </Col>
                <Col sm={12} md={{span: 4, offset: 1}}>
                    <Card border="danger">
                        <Card.Header className="text-white" style={{ backgroundColor: "#dc3545", fontSize: '1.4rem', textAlign: "center" }}>Decessi Giornalieri</Card.Header>
                        <Card.Body>
                        <Card.Title className="text-danger" style={NumTextStyle}>{props.data.todayDeaths}</Card.Title>
                        
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default WorldData
