import React, { useState, useEffect } from 'react'
import Loading from '../commons/Loading'
import WorldData from './chunks/WorldData'
import SingleCountry from "./chunks/SingleCountry";
import Axios from 'axios';

function Home() {
    const [worldData, setWorldData] = useState([]);
    const [countries, setCountries] = useState([]);
    const [loading, setLoading] = useState(true);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    useEffect(() => {
        
        Axios.all([
            Axios.get("https://corona.lmao.ninja/v2/all"),
            Axios.get("https://corona.lmao.ninja/v2/countries"),
        ])
        .then((response) => {
            setWorldData(response[0].data);
            setCountries(response[1].data);
        })
        .catch((err) => {
            console.log(err);
        })
        .finally( async () => {
            await delay(1500);
            setLoading(false);
        })
        
    }, []);

    console.log(worldData);
    return (
       <div style={{ paddingBottom: "39px"}}>
           {loading === true && (
               <Loading />
           )}
           {loading === false && (
               <>
               <h2 style={{ textAlign: "center", textTransform: "uppercase", paddingTop: "30px" }}>Dati Globali</h2>
               <WorldData data={worldData} />
               <h2 style={{ textAlign: "center", textTransform: "uppercase", paddingTop: "30px" }}>Scegli Nazione</h2>
               <SingleCountry countries={countries} />
               </>
           )}
       </div>
    );
}

export default Home;